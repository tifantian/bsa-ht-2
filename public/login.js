window.onload = () => {

    // alert('login.js');
    const   loginField = document.getElementById('login-input'),
            passwordField = document.getElementById('password-input'),
            submitButton = document.getElementById('submit-btn'),
            warning = document.getElementsByClassName('warn')[0];

    submitButton.addEventListener('click', event => {
        fetch('/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                login: loginField.value,
                password: passwordField.value
            })
        })
        .then(res => {
            res.json()
            .then(body => {
                if(body.auth) {
                    localStorage.setItem('jwt', body.token);
                    location.replace('/gamefield');
                } else {
                    warning.style.display = 'block';
                }
            })
        })
        .catch(err => {
            throw new Error('request went wrong');
        });
    });

    loginField.addEventListener('click', event => {
        warning.style.display = '';
    });

    passwordField.addEventListener('click', event => {
        warning.style.display = '';
    });

}