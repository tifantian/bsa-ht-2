window.onload = () => {
  const jwt = localStorage.getItem('jwt');
  if (!jwt) {
    location.replace('/login');
  } else {
    var socket = io.connect('http://localhost:7777');
    socket.emit('newPlayer', { token: jwt });
    let textForGame;
    socket.on('exit', () => {
      location.replace('/gamefield');
    });

    const textField = document.querySelector('.field-with-text'),
      minutesLabel = document.querySelector('.minutes'),
      secondsLabel = document.querySelector('.seconds'),
      countSymbol = document.querySelector('.count-of-symbol'),
      fieldWithUsersInfo = document.querySelector('.field-with-users-info'),
      fieldMessage = document.querySelector('.message-for-all'),
      shwoResults = document.querySelector('.show-results');

    socket.on('changePage', () => {
      location.replace('/gamefield');
    });
    socket.on('getTextId', payload => {
      const { randomValue } = payload;
      getTextAsync(randomValue)
        .then(data => {
          getTextForGame(data);
        });
    });

    socket.on('gameFinished', payload => {
      const { score, user, resultTime } = payload;
      shwoResults.innerHTML = `Выиграл ${user}. Символов: ${score}, Время: ${resultTime}
Перезапустите страницу для новой игры`;
      // document.body.append(res);
      // clearInterval(timerID);
      // clearInterval(finishTimerID);
    });

    socket.on('showMessageWait', () => {
      fieldMessage.innerHTML = 'Пока что идет гонка, подождите пж :)';
    });

    socket.on('startGame', () => {
      startGame(textForGame);
    });

    socket.on('newPlayer', payload => {
      let { objectUsers, secondsToStart } = payload;
      const arrayUsers = Object.keys(objectUsers);
      while (fieldWithUsersInfo.firstChild) {
        fieldWithUsersInfo.removeChild(fieldWithUsersInfo.firstChild);
      }
      fieldMessage.innerHTML = `Секунд до начала битвы: ${secondsToStart}`;
      let timerID = setInterval(() => {
        secondsToStart--;
        fieldMessage.innerHTML = `Секунд до начала битвы: ${secondsToStart}`;
        if (secondsToStart == 0) {
          fieldMessage.innerHTML = "Понеслась !!!";
          clearInterval(timerID);
        }
      }, 1000);
      for (let i = 0; i < arrayUsers.length; i++) {
        const userLogin = arrayUsers[i];                        // Юзер логин с пейлоад
        const blockUserInfo = document.createElement('div');    // получили создали блок с юзером
        blockUserInfo.classList.add(`block-${userLogin}`);      // Добавили уникальный класс
        const loginName = document.createElement('div');        // добавили поле логин нейм
        const loginScore = document.createElement('div');       // добавили поле loginscore
        loginScore.classList.add(`score-${userLogin}`);         // класс блок юзер инфо
        loginName.textContent = userLogin;                      // Заполнили туда юзера
        loginScore.textContent = 0;                             // Поле с очками = 0
        blockUserInfo.append(loginName, loginScore);            // запушили всю бурду в blockUserInfo
        fieldWithUsersInfo.append(blockUserInfo);               // а потом в textField
      }
    });

    socket.on("deletePlayer", payload => {
      const userLogin = payload;
      const deleteElem = document.querySelector(`.block-${userLogin}`);
      deleteElem.classList.add('disconnect-user');
    });

    let timerID;
    let finishTimerID;
    function startGame(text) {
      const arraySymbols = text.split(""); // 2. Массив символов
      let spansArray = [];
      for (let i = 0; i < arraySymbols.length; i++) { // 3.
        const span = document.createElement('span');
        span.textContent = arraySymbols[i];
        span.classList.add('waiting'); // 4.
        spansArray[i] = span;
      }

      textField.append(...spansArray);

      let counter = 0;
      let wrongSymbols = 0;
      spansArray[counter].classList.add('next');
      const timeToPlay = 30; // Время для игры
      let totalSeconds = timeToPlay;
      setTime();
      timerID = setInterval(setTime, 1000);
      window.addEventListener('keypress', keypressListener, true);
      window.addEventListener("keydown", KeyCheck, false);

      finishTimerID = setInterval(function () {
        if (totalSeconds === 0) {
          finishedGame();
          clearInterval(finishTimerID);
        }
      }, 1000);

      function KeyCheck(event) {
        if (event.keyCode == 8 && wrongSymbols !== 0) {
          counter--;
          wrongSymbols--;
          spansArray[counter].classList.remove('mistake');
          spansArray[counter].classList.add('waiting');
        } else {
          return;
        }
      }

      function keypressListener(event) {
        const inputSymbol = getChar(event);
        if ((inputSymbol === arraySymbols[counter]) && wrongSymbols == 0) {
          spansArray[counter].classList.remove('waiting', 'mistake', 'next');
          spansArray[counter].classList.add('right');
          if (counter <= spansArray.length - 2) {
            spansArray[counter + 1].classList.add('next');
          }
          counter++;
        } else {
          spansArray[counter].classList.remove('waiting');
          spansArray[counter].classList.add('mistake');
          counter++;
          wrongSymbols++;
        }
        if ((counter == spansArray.length) && spansArray[spansArray.length - 1].classList.contains('right')) {
          clearInterval(finishTimerID);
          finishedGame();
        }
        const score = counter - wrongSymbols;
        socket.emit('countScores', { score: score, token: jwt });
      }
      function finishedGame() {
        clearInterval(timerID);
        const sek = secondsLabel.textContent;
        const min = minutesLabel.textContent;

        window.removeEventListener('keypress', keypressListener, true);
        window.removeEventListener("keydown", KeyCheck, false);
        const score = counter - wrongSymbols;
        socket.emit('gameFinished', { timeToPlay, totalSeconds, token: jwt, score });
      }
      //timer
      function setTime() {
        --totalSeconds;
        secondsLabel.innerHTML = pad(totalSeconds % 60);
        minutesLabel.innerHTML = pad(parseInt(totalSeconds / 60));
      }

      function pad(val) {
        var valString = val + "";
        if (valString.length < 2) {
          return "0" + valString;
        } else {
          return valString;
        }
      }
      //**** */

      socket.on('countScores', payload => {
        const { user, score } = payload;
        document.querySelector(`.score-${user}`).textContent = score;
      });

    } // конец функции с игрой

    function getChar(event) {
      if (event.which == null) { // IE
        if (event.keyCode < 32) return null;
        return String.fromCharCode(event.keyCode)
      }

      if (event.which != 0 && event.charCode != 0) {
        if (event.which < 32) return null;
        return String.fromCharCode(event.which);
      }

      return null;
    }
    //
    async function getTextAsync(randomValue) {
      let data;
      const response = await fetch('/texts', {
        method: "GET",
        headers: {
          "Authorization": `Bearer ${jwt}`,
        }
      })
        .then(r => r.json())
        .then(res => {
          data = res;
        })
        .catch(err => console.log("ERROR  " + err));
      const lengthTextsArray = Object.keys(data).length;
      // const randomValue = 1 + Math.floor(Math.random() * (lengthTextsArray));
      return data[`${randomValue}`];
    }
    function getTextForGame(data) {
      textForGame = data;
    }

  }
}