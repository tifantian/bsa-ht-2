const path = require('path');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
// const morgan = require('morgan');
const passport = require('passport');
const cors = require('cors');
const indexRouter = require('./routes/index.js');
const loginRouter = require('./routes/login.js');
const gameFieldRouter = require('./routes/gamefield.js')
const giveTextRouter = require('./routes/texts.js');
const startGame = require('./routes/startgame.js')

require('./passport.config');

server.listen(7777);
const texts = require('./textsJSON/texts');
console.log('Listen PORT localhost:7777');

// app.use(morgan('dev'));
app.use(cors());
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());

app.use('/', indexRouter);
app.use('/login', loginRouter);
app.use('/gamefield', gameFieldRouter);
app.use('/texts', giveTextRouter);
app.use('/startgame', startGame);


const objectUsers = {}; // сначала было конст

const defaultValueOfSeconds = 4;  /// Тут можно поменять таймер
const lengthTextsArray = Object.keys(texts).length;
let randomValue = 1 + Math.floor(Math.random() * (lengthTextsArray));
let isGameStarted = false;
let secondsToStart = defaultValueOfSeconds;
let timerID;
const allSockets = [];
const objLoginSocket = {};

io.on('connection', socket => {
  allSockets.push(socket);
  socket.on('newPlayer', payload => {
    const { token } = payload;
    const userLogin = jwt.decode(token).login;
    if (!userLogin) {
      socket.emit('Exit');
      return;
    }
    // вылавливание сокета для дисконекта
    const index = allSockets.indexOf(socket);
    allSockets[index].userName = userLogin;
    objLoginSocket[userLogin] = socket;

    if (Object.keys(objectUsers).length == 0) {
      makeNewTimer(secondsToStart);
      // randomValue = 1 + Math.floor(Math.random() * (lengthTextsArray)); 
    }
    objectUsers[userLogin] = 0;
    console.log(`${userLogin} connect`);
    if (isGameStarted) {
      socket.emit('showMessageWait');
      return;
    }
    socket.broadcast.emit('newPlayer', { objectUsers, secondsToStart }); // Для всех кроме текущего
    socket.emit('newPlayer', { objectUsers, secondsToStart }); // Для всех
  });

  socket.emit('getTextId', { randomValue });

  socket.on('gameFinished', payload => {
    isGameStarted = false;
    const { timeToPlay, totalSeconds, token, score } = payload;
    const userLogin = jwt.decode(token).login;
    const resultTime = timeToPlay - totalSeconds;
    socket.broadcast.emit('gameFinished', { score, user: userLogin, resultTime });
    socket.emit('gameFinished', { score, user: userLogin, resultTime });
  });

  socket.on('countScores', payload => { // количество введенных символом
    const { score, token } = payload;
    const userLogin = jwt.decode(token).login;
    objectUsers[userLogin] = score;
    socket.broadcast.emit('countScores', { score, user: userLogin });
    socket.emit('countScores', { score, user: userLogin });
  });

  socket.on("disconnect", () => {
    clearInterval(timerID);
    const index = allSockets.indexOf(socket);
    const disconnectedUser = allSockets[index].userName;

    delete objectUsers[disconnectedUser];
    console.log(`${disconnectedUser} disconnect`);
    if (Object.keys(objectUsers).length === 0) {
      isGameStarted = false;
      secondsToStart = defaultValueOfSeconds;
    }

    socket.broadcast.emit('deletePlayer', disconnectedUser);
    socket.emit('deletePlayer', disconnectedUser);

    // Когда все дисконектнулись сгенерировать новую 
  });

  function startGame() {
    isGameStarted = true;
    socket.broadcast.emit('startGame');
    socket.emit('startGame');
  }

  function makeNewTimer(timeInSeconds) {
    secondsToStart = timeInSeconds;
    timerID = setInterval(() => {
      secondsToStart--;
      if (secondsToStart === 0) {
        secondsToStart = defaultValueOfSeconds;
        startGame();
        clearInterval(timerID);
      }
    }, 1000);
  }
});

